import { Lines } from '@/types/line'
import { Stops, StopsDto } from '@/types/stop'
import { createStore } from 'vuex'
import axios from "axios";

export default createStore<{
    stops: Stops
    lines: Lines
    fetchingState: 'idle' | 'fetching' | 'success' | 'error'
}>({
    state: {
        stops: [],
        lines: {},
        fetchingState: 'idle',
    },
    getters: {
        lines: state => state.lines,
        stops: state => state.stops,
        fetchingState: state => state.fetchingState,
    },
    mutations: {
        setLines(state, lines) {
            state.lines = lines
        },
        setStops(state, stops) {
            state.stops = stops
        },
        setFetching(state, fetchingState: 'idle' | 'fetching' | 'success' | 'error') {
            state.fetchingState = fetchingState
        }
    },
    actions: {
        async fetchStops({ commit }) {
            commit('setFetching', 'fetching')
            let stopsDto: StopsDto = []

            try {
                const response = await axios.get('http://localhost:3000/stops')
                stopsDto = response.data
            } catch (e) {
                commit('setFetching', 'error')
                return
            }
            const stops: Stops = []
            const lines: Lines = {}
            for (const stopDto of stopsDto) {
                const { line, stop, order, time } = stopDto
                // We can do this only if we have 24h time format for hours.
                const timeStamp = parseInt(time.split(":").join(''))

                if (!lines[line]) {
                    lines[line] = []
                }
                if (!lines[line][order - 1]) {
                    lines[line][order - 1] = { name: stop, times: [] }
                }
                // Inserting to array by timestamps will guarantee order.
                // Filter out undefined values in component computed.
                lines[line][order - 1].times[timeStamp] = time

                if (!stops.includes(stop)) stops.push(stop)
            }

            commit('setStops', stops)
            commit('setLines', lines)
            commit('setFetching', 'success')
        },
    },
})
