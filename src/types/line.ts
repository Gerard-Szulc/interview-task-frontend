import { StopId } from "./stop"

export type LineId = number
export type Line = { name: StopId, times: string[] }[]
export type Lines = Record<LineId, Line>