export type StopDto = {
    line: number
    stop: string
    order: number
    time: string
};
export type StopsDto = StopDto[];

export type StopId = string

export type Stops = StopId[]
