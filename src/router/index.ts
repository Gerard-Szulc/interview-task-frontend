import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'bus-lines',
        component: () => import(/* webpackChunkName: "bus-lines" */ '../views/bus-lines-view.vue'),
        children: [
            {
                path: '/:lineId',
                name: 'bus-line-stops',
                component: () => import(/* webpackChunkName: "bus-line-stops" */ '../views/bus-line-stops-view.vue'),
                children: [
                    {
                        path: '/:lineId/:stopId',
                        name: 'bus-line-stop-times',
                        component: () => import(/* webpackChunkName: "bus-line-stop-times" */ '../views/bus-line-stop-times-view.vue')
                    },

                ]
            },
        ]
    },
    {
        path: '/stops',
        name: 'stops',
        component: () => import(/* webpackChunkName: "stops-view" */ '../views/stops-view.vue')
    }]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
