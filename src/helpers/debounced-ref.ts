import { customRef } from "vue";

// of course it's copied from vue docs examples, it suits here perfectly
export function useDebouncedRef(value: string, delay = 200) {
    let timeout: ReturnType<typeof setTimeout>
    return customRef((track, trigger) => {
        return {
            get() {
                track()
                return value
            },
            set(newValue: string) {
                clearTimeout(timeout)
                timeout = setTimeout(() => {
                    value = newValue
                    trigger()
                }, delay)
            }
        }
    })
}
