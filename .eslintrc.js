module.exports = {
    root: true,
    env: {
        node: true
    },
    'extends': [
        'plugin:vue/vue3-essential',
        'eslint:recommended',
        '@vue/typescript/recommended'
    ],
    parserOptions: {
        ecmaVersion: 2020
    },
    rules: {
        'no-console': 'error',
        "comma-spacing": 2,
        "space-unary-ops": 2,
        "space-infix-ops": 2,
        "space-in-parens": 2,
        "space-before-function-paren": [
            "error",
            {
                "anonymous": "always",
                "named": "never",
                "asyncArrow": "always"
            }
        ],
        "space-before-blocks": 2,
        "sort-vars": 2,
        "object-curly-newline": 2,
        "object-curly-spacing": ["error", "always"],
        "no-trailing-spaces": 2,
        "indent": ["warn", 4, { "SwitchCase": 1, "ignoredNodes": ["PropertyDefinition"] }],
        "@typescript-eslint/no-explicit-any": 2,
    }
}
